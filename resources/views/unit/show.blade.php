@extends('layouts.app')
@section('content')

    <h1>Detalle de unidad</h1>
    <p>Id: {{ $unit->id }}</p>
    <p>Horas: {{ $unit->hours }}</p>
    <p>Número: {{ $unit->number }}</p>
    <p>Título: {{ $unit->title }}</p>
    <br><a href="/units">Volver</a>
@endsection('content')