@extends('layouts.app')
@section('content')

    <h1>Editar datos de unidad</h1>
    <div class="form">
    <form  action="/units/{{ $unit->id }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">

    <div class="form-group">
        <label>Id: </label>
        <input type="text" name="id" value="{{ $unit->id }}" readonly="readonly">
    </div>
    <div class="form-group">
        <label>Horas: </label>
        <input type="text" name="hours" value="{{ old('hours',  $unit->hours) }}">
        {{ $errors->first('hours') }}
    </div>
    <div class="form-group">
        <label>Título </label>
        <input type="text" name="title" value="{{ old('title',  $unit->title) }}">
        {{ $errors->first('title') }}
    </div>
    <input type="submit" value="Guardar">
    </form>
    <br><a href="/units">Volver</a>
    </div>

@endsection('content')