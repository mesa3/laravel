@extends('layouts.app')

@section('menu')
    @include('layouts.menu');
@endsection('menu')

@section('content')
    <h1>Lista de unidades</h1>
    @can('create', 'App\Unit')
        <a href="/units/create">Nuevo</a>
    @endcan


    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Horas</th>
                <th>Título</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($units as $unit)
            <tr>
                <td>  {{ $unit->id }} </td>
                <td>  {{ $unit->hours }} </td>
                <td>  {{ $unit->title }} </td>
                <td>  
                    <form method="post" action="/units/{{ $unit->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}

                        @can('delete', $unit)
                        <input type="submit" value="Borrar">
                        @endcan

                        @can('update', $unit)
                        <a href="/units/{{ $unit->id }}/edit">Editar</a>
                        @endcan

                        @can('view', $unit)
                        <a href="/units/{{ $unit->id }}"> Ver </a>
                        @endcan
                    </form>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $units->render() }}

@endsection('content')
