@extends('layouts.app')
@section('content')


    <h1>Crear unidad</h1>
    <div class="form">
    <form  action="/units" method="post">
    {{ csrf_field() }}

    <div class="form-group">
        <label>Horas: </label>
        <input type="text" name="hours" value="{{ old('hours') }}">
        {{ $errors->first('hours') }}
    </div>
    <div class="form-group">
        <label>Título: </label>
        <input type="text" name="title" value="{{ old('title') }}">
        {{ $errors->first('title') }}
    </div>
    <div class="form-group">
        <label>Usuario: </label>
        <input type="text" name="user_id" value="{{ $user_id }}" readonly>
        {{ $errors->first('user_id') }}
    </div>

    <input type="submit" value="Guardar">
    </form>
    <br><a href="/units">Volver</a>
    </div>


@endsection('content')

