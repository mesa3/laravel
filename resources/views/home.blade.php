@extends('layouts.app')

@section('menu')
    @include('layouts.menu');
@endsection('menu')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">¡Bienvenido/a a nuestra aplicación!</div>

                <div class="panel-body">
                    ¡Has sido logeado!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
