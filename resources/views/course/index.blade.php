@extends('layouts.app')

@section('menu')
    @include('layouts.menu');
@endsection('menu')

@section('content')
    <h1>Lista de cursos</h1>
    @can('create', 'App\Course')
        <a href="/courses/create">Nuevo</a>
    @endcan


    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Año</th>
                <th>Día inicio</th>
                <th>Día fin</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($courses as $course)
            <tr>
                <td>  {{ $course->id }} </td>
                <td>  {{ $course->year }} </td>
                <td>  {{ $course->date_ini }} </td>
                <td>  {{ $course->date_end }} </td>
                <td>  
                    <form method="post" action="/courses/{{ $course->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}

                        @can('delete', $course)
                        <input type="submit" value="Borrar">
                        @endcan

                        @can('update', $course)
                        <a href="/courses/{{ $course->id }}/edit">Editar</a>
                        @endcan

                        @can('view', $course)
                        <a href="/courses/{{ $course->id }}"> Ver </a>
                        @endcan
                    </form>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $courses->render() }}

@endsection('content')