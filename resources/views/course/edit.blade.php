@extends('layouts.app')
@section('content')

    <h1>Editar datos de curso</h1>
    <div class="form">
    <form  action="/courses/{{ $course->id }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">

    <div class="form-group">
        <label>Id: </label>
        <input type="text" name="id" value="{{ $course->id }}" readonly="readonly">
    </div>
    <div class="form-group">
        <label>Año: </label>
        <input type="text" name="year" value="{{ old('year',  $course->year) }}">
        {{ $errors->first('year') }}
    </div>
    <div class="form-group">
        <label>Fecha de inicio: </label>
        <input type="text" name="date_ini" id="datepicker1" value="{{ old('date_ini',  $course->date_ini) }}">
        {{ $errors->first('date_ini') }}
    </div>
    <div class="form-group">
        <label>Fecha de fin: </label>
        <input type="text" name="date_end" id="datepicker2" value="{{ old('date_end',  $course->date_end) }}">
        {{ $errors->first('date_end') }}
    </div>
    <input type="submit" value="Guardar">
    </form>
    <br><a href="/courses">Volver</a>
    </div>

@endsection('content')

@section('scripts')

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>

    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

    <script >
    $(document).ready(function() {
        $("#datepicker1").datepicker({

                dateFormat: "yy-mm-dd",

                dayNames: [ "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" ],

                dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],

                firstDay: 1,

                gotoCurrent: true,

                monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre" ]

            });
        $("#datepicker2").datepicker({

                dateFormat: "yy-mm-dd",

                dayNames: [ "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" ],

                dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],

                firstDay: 1,

                gotoCurrent: true,

                monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre" ]

            });
    });
    </script>
@endsection('scripts')