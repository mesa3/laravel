@extends('layouts.app')
@section('content')

    <h1>Detalle de curso</h1>
    <p>Id: {{ $course->id }}</p>
    <p>Año: {{ $course->year }}</p>
    <p>Fecha inicio: {{ $course->date_ini }}</p>
    <p>Fecha fin: {{ $course->date_end }}</p>
    <br><a href="/courses">Volver</a>

@endsection('content')