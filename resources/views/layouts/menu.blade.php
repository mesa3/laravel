
    <div style=" margin-left: 30px; background-color:white; border:solid 0.5px;border-color: lightgrey">
        <ul class="nav nav-pills nav-stacked">
            @can('create', 'App\User')
            <li><a href="/users">Usuarios</a></li>
            @endcan
            <li><a href="/classbooks">Cuaderno</a></li>
            <li><a href="/courses">Cursos</a></li>
            <li><a href="/classes">Clases</a></li>
            <li><a href="/units">Unidades</a></li>
            <li><a href="/days/ajax">Calendario</a></li>

        </ul>
    </div>
