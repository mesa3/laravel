@extends('layouts.app')

@section('content')
    <h1>Calendario</h1>
    <table>
    <thead>
        <tr>
            <th>Date</th>
        </tr>
    </thead>
    <tbody>
    @foreach($days as $day)
        <tr>
            
            @if ($day->holiday)
                <td style="background-color: lightgreen">  {{ substr($day->date, 8) }} </td>
            @else
                <td>  {{ substr($day->date, 8) }} </td>
            @endif
        </tr>
    @endforeach
    </tbody>
    </table>

    {{ $days->render() }}

@endsection('content')