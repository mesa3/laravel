@extends('layouts.app')

@section('menu')
    @include('layouts.menu');
@endsection('menu')

@section('styles')
    <style type="text/css">
        table {
            border-collapse: collapse;
        }

        table, td, th {
            border: solid, 0.5px, grey;
        }

        td:hover {
            background-color: lightgrey;
        }

        #prev {
            float: left;
        }

        #next {
            float:right;
        }

        .holiday {
            background-color: lightgreen;
        }

    </style>
@stop
@section('content')

    <h1>Calendario</h1>
    <table class="table">
        <p><span id="month">Month</span> of <span id = "year">Year</span></p>
        <thead>
            <tr>
                <th>Mon</th>
                <th>Tue</th>
                <th>Wed</th>
                <th>Thu</th>
                <th>Fri</th>
                <th>Sat</th>
                <th>Sun</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td id = "1Mon" onclick="toggleHoliday(this)"></td>
                <td id = "1Tue" onclick="toggleHoliday(this)" ></td>
                <td id = "1Wed" onclick="toggleHoliday(this)" ></td>
                <td id = "1Thu" onclick="toggleHoliday(this)" ></td>
                <td id = "1Fri" onclick="toggleHoliday(this)" ></td>
                <td id = "1Sat" onclick="toggleHoliday(this)" ></td>
                <td id = "1Sun" onclick="toggleHoliday(this)" ></td>
            </tr>
            <tr>
                <td id = "2Mon" onclick="toggleHoliday(this)" ></td>
                <td id = "2Tue" onclick="toggleHoliday(this)" ></td>
                <td id = "2Wed" onclick="toggleHoliday(this)" ></td>
                <td id = "2Thu" onclick="toggleHoliday(this)" ></td>
                <td id = "2Fri" onclick="toggleHoliday(this)" ></td>
                <td id = "2Sat" onclick="toggleHoliday(this)" ></td>
                <td id = "2Sun" onclick="toggleHoliday(this)" ></td>
            </tr>
            <tr>
                <td id = "3Mon" onclick="toggleHoliday(this)" ></td>
                <td id = "3Tue" onclick="toggleHoliday(this)" ></td>
                <td id = "3Wed" onclick="toggleHoliday(this)" ></td>
                <td id = "3Thu" onclick="toggleHoliday(this)" ></td>
                <td id = "3Fri" onclick="toggleHoliday(this)" ></td>
                <td id = "3Sat" onclick="toggleHoliday(this)" ></td>
                <td id = "3Sun" onclick="toggleHoliday(this)" ></td>
            </tr>
            <tr>
                <td id = "4Mon" onclick="toggleHoliday(this)" ></td>
                <td id = "4Tue" onclick="toggleHoliday(this)" ></td>
                <td id = "4Wed" onclick="toggleHoliday(this)" ></td>
                <td id = "4Thu" onclick="toggleHoliday(this)" ></td>
                <td id = "4Fri" onclick="toggleHoliday(this)" ></td>
                <td id = "4Sat" onclick="toggleHoliday(this)" ></td>
                <td id = "4Sun" onclick="toggleHoliday(this)" ></td>
            </tr>
            <tr>
                <td id = "5Mon" onclick="toggleHoliday(this)" ></td>
                <td id = "5Tue" onclick="toggleHoliday(this)" ></td>
                <td id = "5Wed" onclick="toggleHoliday(this)" ></td>
                <td id = "5Thu" onclick="toggleHoliday(this)" ></td>
                <td id = "5Fri" onclick="toggleHoliday(this)" ></td>
                <td id = "5Sat" onclick="toggleHoliday(this)" ></td>
                <td id = "5Sun" onclick="toggleHoliday(this)" ></td>
            </tr>
            <tr>
                <td id = "6Mon" onclick="toggleHoliday(this)" ></td>
                <td id = "6Tue" onclick="toggleHoliday(this)" ></td>
                <td id = "6Wed" onclick="toggleHoliday(this)" ></td>
                <td id = "6Thu" onclick="toggleHoliday(this)" ></td>
                <td id = "6Fri" onclick="toggleHoliday(this)" ></td>
                <td id = "6Sat" onclick="toggleHoliday(this)" ></td>
                <td id = "6Sun" onclick="toggleHoliday(this)" ></td>
            </tr>
        </tbody>
    </table>
    <button id = "prev" onClick="prev()">Prev</button>
    <button id = "next" onClick="next()">Next</button>

@endsection('content')

@section('scripts')
  <script type="text/javascript" src="/js/calendarSetup.js"></script>
  <script type="text/javascript" src="/js/calendarPagination.js"></script>
  <script type="text/javascript" src="/js/holiday.js"></script>
@stop