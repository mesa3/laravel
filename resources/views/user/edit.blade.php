@extends('layouts.app')
@section('content')

    <h1>Editar datos de usuario</h1>
    <div class="form">
    <form  action="/users/{{ $user->id }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">

    <div class="form-group">
        <label>Id: </label>
        <input type="text" name="id" value="{{ $user->id }}" readonly="readonly">
    </div>
    <div class="form-group">
        <label>Name: </label>
        <input type="text" name="name" value="{{ old('name',  $user->name) }}">
        {{ $errors->first('name') }}
    </div>
    <div class="form-group">
        <label>Surname: </label>
        <input type="text" name="surname" value="{{ old('surname',  $user->surname) }}">
        {{ $errors->first('surname') }}
    </div>
    <div class="form-group">
        <label>Email: </label>
        <input type="text" name="email" value="{{ old('email',  $user->email) }}">
        {{ $errors->first('email') }}
    </div>
    <div class="form-group">
        <label>Password: </label>
        <input type="password" name="password" value="{{ old('password',  $user->password) }}" readonly="readonly">
        {{ $errors->first('password') }}
    </div>
    <input type="submit" value="Guardar">
    </form>
    <br><a href="/users">Volver</a>
    </div>

@endsection('content')