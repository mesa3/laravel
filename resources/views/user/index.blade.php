@extends('layouts.app')

@section('menu')
    @include('layouts.menu');
@endsection('menu')

@section('content')
    <h1>Lista de usuarios</h1>
    @can('create', 'App\User')
        <a href="/users/create">Nuevo</a>
    @endcan


    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($users as $user)
            <tr>
                <td>  {{ $user->id }} </td>
                <td>  {{ $user->name }} </td>
                <td>  {{ $user->surname }} </td>
                <td>  {{ $user->email }} </td>
                <td>  
                    <form method="post" action="/users/{{ $user->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}

                        @can('delete', $user)
                        <input type="submit" value="Borrar">
                        @endcan

                        @can('update', $user)
                        <a href="/users/{{ $user->id }}/edit">Editar</a>
                        @endcan

                        @can('view', $user)
                        <a href="/users/{{ $user->id }}"> Ver </a>
                        @endcan
                    </form>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $users->render() }}

@endsection('content')