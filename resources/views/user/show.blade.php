@extends('layouts.app')
@section('content')

    <h1>Detalle de usuario</h1>
    <p>Id: {{ $user->id }}</p>
    <p>Name: {{ $user->name }}</p>
    <p>Surname: {{ $user->surname }}</p>
    <p>Email: {{ $user->email }}</p>
    <br><a href="/users">Volver</a>
@endsection('content')