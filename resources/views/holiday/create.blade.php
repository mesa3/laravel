@extends('layouts.app')
@section('content')

    <h1>Crear datos de vacaciones</h1>
    <div class="form">
    <form  action="/holidays" method="post">
    {{ csrf_field() }}


    <div class="form-group">
        <label>Holiday: </label>
        <input type="text" name="holiday" value="{{ old('holiday') }}">
        {{ $errors->first('code') }}
    </div>
    <input type="submit" value="Guardar">
    </form>
    </div>

@endsection('content')