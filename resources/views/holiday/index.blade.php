@extends('layouts.app')

@section('menu')
    @include('layouts.menu');
@endsection('menu')

@section('content')
    <h1>Lista de vacaciones</h1>
    @can('create', 'App\Holiday')
        <a href="/holidays/create">Nuevo</a>
    @else
        No puedes dar altas!!
    @endcan


    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Holiday</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($holidays as $holiday)
            <tr>
                <td>  {{ $holiday->id }} </td>
                <td>  {{ $holiday->holiday }} </td>
                <td>  {{ $holiday->date }} </td>
                <td>  
                    <form method="post" action="/holidays/{{ $holiday->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}

                        @can('delete', $holiday)
                        <input type="submit" value="Borrar">
                        @endcan
                    </form>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $holidays->render() }}

@endsection('content')