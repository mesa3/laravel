@extends('layouts.app')
@section('content')

  



    <h1>Crear datos de clase</h1>
    <div class="form">
    <form  action="/classes" method="post">
    {{ csrf_field() }}


    <div class="form-group">
        <label>Horas: </label>
        <input type="text" name="hours" value="{{ old('hours') }}">
        {{ $errors->first('hours') }}
    </div>
    <div class="form-group">
        <label>Plan: </label>
        <input type="text" name="plan" value="{{ old('plan') }}">
        {{ $errors->first('plan') }}
    </div>
    <div class="form-group">
        <label>Hecho: </label>
        <input type="checkbox" name="done" value="yes">
    </div>
    <div class="form-group">
        <label>Fecha: </label>
        <input type="text" name="date" id="datepicker"  value="{{ old('date') }}">
        {{ $errors->first('date') }}
    </div>
    <div class="form-group">
        <label>Usuario: </label>
        <input type="text" name="user_id" value="{{ $user_id }}" readonly>
        {{ $errors->first('user_id') }}
    </div>
    <input type="submit" value="Guardar">
    </form>
    <br><a href="/classes">Volver</a>
    </div>

    
    

@endsection('content')
@section('scripts')

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>

    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

    <script >
    $(document).ready(function() {
        $("#datepicker").datepicker({

                dateFormat: "yy-mm-dd",

                dayNames: [ "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" ],

                dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],

                firstDay: 1,

                gotoCurrent: true,

                monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre" ]

            });
    });
    </script>
@endsection('scripts')
