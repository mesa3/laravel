@extends('layouts.app')
@section('content')

    <h1>Detalle de clase</h1>
    <p>Id: {{ $clase->id }}</p>
    <p>Horas: {{ $clase->hours }}</p>
    <p>Plan: {{ $clase->plan }}</p>
    <p>Hecho: {{ $clase->done }}</p>
    <p>Fecha: {{ $clase->date }}</p>
    <br><a href="/classes">Volver</a>

@endsection('content')