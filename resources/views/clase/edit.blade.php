@extends('layouts.app')
@section('content')

    <h1>Editar datos de clase</h1>
    <div class="form">
    <form  action="/classes/{{ $clase->id }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">

    <div class="form-group">
        <label>Id: </label>
        <input type="text" name="id" value="{{ $clase->id }}" readonly="readonly">
    </div>
    <div class="form-group">
        <label>Horas: </label>
        <input type="text" name="hours" value="{{ old('hours',  $clase->hours) }}">
        {{ $errors->first('hours') }}
    </div>
    <div class="form-group">
        <label>Plan: </label>
        <input type="text" name="plan" value="{{ old('plan',  $clase->plan) }}">
        {{ $errors->first('plan') }}
    </div>
    <div class="form-group">
        <label>Hecho: </label>
        <input type="checkbox" name="done" {{ ($clase->done == "yes") ? 'checked' : '' }} value="yes">
        {{ $errors->first('done') }}
    </div>
    <div class="form-group">
        <label>Fecha: </label>
        <input type="text" name="date_end" id="datepicker" value="{{ old('date',  $clase->date) }}">
        {{ $errors->first('date') }}
    </div>
    <input type="submit" value="Guardar">
    </form>
    <br><a href="/classes">Volver</a>
    </div>

@endsection('content')
@section('scripts')

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>

    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

    <script >
    $(document).ready(function() {
        $("#datepicker").datepicker({

                dateFormat: "yy-mm-dd",

                dayNames: [ "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" ],

                dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],

                firstDay: 1,

                gotoCurrent: true,

                monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre" ]

            });
    });
    </script>
@endsection('scripts')
