@extends('layouts.app')

@section('menu')
    @include('layouts.menu');
@endsection('menu')

@section('content')
    <h1>Lista de clases</h1>
    @can('create', 'App\Clase')
        <a href="/classes/create">Nuevo</a>
    @endcan


    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Hours</th>
                <th>Plan</th>
                <th>Done</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($clases as $clase)
            <tr>
                <td>  {{ $clase->id }} </td>
                <td>  {{ $clase->hours }} </td>
                <td>  {{ $clase->plan }} </td>
                <td>  {{ $clase->done }} </td>
                <td>  {{ $clase->date }} </td>
                <td>  
                    <form method="post" action="/classes/{{ $clase->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}

                        @can('delete', $clase)
                        <input type="submit" value="Borrar">
                        @endcan

                        @can('update', $clase)
                        <a href="/classes/{{ $clase->id }}/edit">Editar</a>
                        @endcan

                        @can('view', $clase)
                        <a href="/classes/{{ $clase->id }}"> Ver </a>
                        @endcan
                    </form>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $clases->render() }}

@endsection('content')