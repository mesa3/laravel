@extends('layouts.app')
@section('content')

    <h1>Editar registro</h1>
    <div class="form">
    <form  action="/classbooks/{{ $classbook->id }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">

    <div class="form-group">
        <label>Id: </label>
        <input type="text" name="id" value="{{ $classbook->id }}" readonly="readonly">
    </div>
    <div class="form-group">
        <label>Módulo: </label>
        <input type="text" name="module" value="{{ old('module',  $classbook->module) }}">
        {{ $errors->first('module') }}
    </div>
    <div class="form-group">
        <label>Grupo: </label>
        <input type="text" name="group" value="{{ old('group',  $classbook->group) }}">
        {{ $errors->first('group') }}
    </div>
    <div class="form-group">
        <label>Comentarios: </label>
        <input type="text" name="comments" value="{{ old('comments',  $classbook->comments) }}">
        {{ $errors->first('comments') }}
    </div>
    <div class="form-group">
        <label>Lunes: </label>
        <input type="checkbox" name="day1" {{ $classbook->day1 ? 'checked' : '' }} value="1">
    </div>
    <div class="form-group">
        <label>Martes: </label>
        <input type="checkbox" name="day2" {{ $classbook->day2 ? 'checked' : '' }} value="1">
    </div>
     <div class="form-group">
        <label>Miercoles: </label>
        <input type="checkbox" name="day3" {{ $classbook->day3 ? 'checked' : '' }} value="1">
    </div>
     <div class="form-group">
        <label>Jueves: </label>
        <input type="checkbox" name="day4" {{ $classbook->day4 ? 'checked' : '' }} value="1">
    </div>
    <div class="form-group">
        <label>Viernes: </label>
        <input type="checkbox" name="day5" {{ $classbook->day5 ? 'checked' : '' }} value="1">
    </div>

    <div class="form-group">
        <label>Curso: </label>
        <select type="select" name="course_id" value="{{ old('course_id') }}">
            @foreach ($courses as $course)
            <option value="{{ $course->id }}">{{ $course->year }}</option>
            @endforeach
        </select>
        {{ $errors->first('course_id') }}
    </div>

    <div class="form-group">
        <label>Clase: </label>
        <select type="select" name="class_id" value="{{ old('class_id') }}">
            @foreach ($classes as $class)
            <option value="{{ $class->id }}">{{ $class->date }}</option>
            @endforeach
        </select>
        {{ $errors->first('class_id') }}
    </div>

    <div class="form-group">
        <label>Unidad: </label>
        <select type="select" name="unit_id" value="{{ old('unit_id') }}">
            @foreach ($units as $unit)
            <option value="{{ $unit->id }}">{{ $unit->title }}</option>
            @endforeach
        </select>
        {{ $errors->first('unit_id') }}
    </div>
    <input type="submit" value="Guardar">
    </form>
    </div>

@endsection('content')