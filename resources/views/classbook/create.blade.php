@extends('layouts.app')

@section('content')

<h1>Crear registro</h1>
<div class="form">
<form action="/classbooks" method="post">
    {{ csrf_field() }}

    <div class="form-group">
        <label>Módulo: </label>
        <input type="text" name="module" value="{{ old('module') }}">
        {{ $errors->first('module') }}
    </div>

    <div class="form-group">
        <label>Grupo: </label>
        <input type="text" name="group" value="{{ old('group') }}">
        {{ $errors->first('group') }}
    </div>

    <div class="form-group">
        <label>Comentarios: </label>
        <input type="text" name="comments" value="{{ old('comments') }}">
        {{ $errors->first('comments') }}
    </div>

    <div class="form-group">
        <label>Lunes: </label>
        <input type="checkbox" name="day1" value="1">
        
    </div>

    <div class="form-group">
        <label>Martes: </label>
        <input type="checkbox" name="day2" value="1">
        
    </div>
 
    <div class="form-group">
        <label>Miercoles: </label>
        <input type="checkbox" name="day3" value="1">
        
    </div>

    <div class="form-group">
        <label>Jueves: </label>
        <input type="checkbox" name="day4" value="1">
        
    </div>

    <div class="form-group">
        <label>Viernes: </label>
        <input type="checkbox" name="day5" value="1">
        
    </div>

    <div class="form-group">
        <label>Curso: </label>
        <select type="select" name="course_id" value="{{ old('course_id') }}">
            @foreach ($courses as $course)
            <option value="{{ $course->id }}">{{ $course->year }}</option>
            @endforeach
        </select>
        {{ $errors->first('course_id') }}
    </div>

    <div class="form-group">
        <label>Clase: </label>
        <select type="select" name="class_id" value="{{ old('class_id') }}">
            @foreach ($classes as $class)
            <option value="{{ $class->id }}">{{ $class->date }}</option>
            @endforeach
        </select>
        {{ $errors->first('class_id') }}
    </div>

    <div class="form-group">
        <label>Unidad: </label>
        <select type="select" name="unit_id" value="{{ old('unit_id') }}">
            @foreach ($units as $unit)
            <option value="{{ $unit->id }}">{{ $unit->title }}</option>
            @endforeach
        </select>
        {{ $errors->first('unit_id') }}
    </div>

    <div class="form-group">
        <label>Usuario: </label>
        <input type="text" name="user_id" value="{{ $user_id }}" readonly>
        {{ $errors->first('user_id') }}
    </div>

    <div class="form-group">
        <input type="submit" value="Guardar">
    </div>
</form>
</div>

@endsection('content')