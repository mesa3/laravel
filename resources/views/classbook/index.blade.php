@extends('layouts.app')
    
@section('menu')
    @include('layouts.menu');
@endsection('menu')

@section('content')

    <h1>Cuaderno</h1>
    @can('create', 'App\Classbook')
        <a href="/classbooks/create">Nuevo</a>
    @endcan
    <table class="table">
        <thead>
            <tr>
                <th>Módulo</th>
                <th>Grupo</th>
                <th>Comentarios</th>
                <th>Mon</th>
                <th>Thu</th>
                <th>Wed</th>
                <th>Tue</th>
                <th>Fri</th>
                <th>Usuario</th>
                <th>Clase</th>
                <th>Curso</th>
                <th>Unidad</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($classbooks as $classbook)
            <tr>
                <td>  {{ $classbook->module }} </td>
                <td>  {{ $classbook->group }} </td>
                <td>  {{ $classbook->comments }} </td>
                @if ($classbook->day1 == true)
                    <td>X</td>
                @else
                    <td></td>
                @endif
                @if ($classbook->day2 == true)
                    <td>X</td>
                @else
                    <td></td>
                @endif
                @if ($classbook->day3 == true)
                    <td>X</td>
                @else
                    <td></td>
                @endif
                @if ($classbook->day4 == true)
                    <td>X</td>
                @else
                    <td></td>
                @endif
                @if ($classbook->day5 == true)
                    <td>X</td>
                @else
                    <td></td>
                @endif
                <td>  {{ $classbook->user->name }} </td>
                <td>  {{ $classbook->class->date }} </td>
                <td>  {{ $classbook->course->year }} </td>
                <td>  {{ $classbook->unit->title }} </td>
                <td>  
                   <form method="post" action="/classbooks/{{ $classbook->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}

                        @can('delete', $classbook)
                        <input type="submit" value="Borrar">
                        @endcan

                        @can('update', $classbook)
                        <a href="/classbooks/{{ $classbook->id }}/edit">Editar</a>
                        @endcan
                        @can('view', $classbook)
                        <a href="/classbooks/{{ $classbook->id }}"> Ver </a>
                        @endcan
                    </form>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $classbooks->render() }}

@endsection('content')
