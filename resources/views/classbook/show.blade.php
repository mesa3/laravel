@extends('layouts.app')
@section('content')

    <h1>Detalle de cuaderno</h1>
    <p>Id: {{ $classbook->id }}</p>
    <p>Módulo: {{ $classbook->module }}</p>
    <p>Grupo: {{ $classbook->group }}</p>
    <p>Comentarios: {{ $classbook->comments }}</p>
    @if ($classbook->day1 == true)
        <p>Lunes: si</p>
    @else
        <p>Lunes: no </p>
    @endif
    @if ($classbook->day2 == true)
        <p>Martes: si</p>
    @else
        <p>Martes: no</p>
    @endif
    @if ($classbook->day3 == true)
        <p>Miercoles: si</p>
    @else
        <p>Miercoles: no</p>
    @endif
    @if ($classbook->day4 == true)
        <p>Jueves: si</p>
    @else
        <p>Jueves: no</p>
    @endif
    @if ($classbook->day5 == true)
        <p>Viernes: si</p>
    @else
        <p>Viernes: no</p>
    @endif
    <p>Usuario: {{ $classbook->user->name }}</p>
    <p>Clase: {{ $classbook->class->date }}</p>
    <p>Curso: {{ $classbook->course->year }}</p>
    <p>Unidad: {{ $classbook->unit->title }}</p>
    


    <br><a href="/classbooks">Volver</a>
@endsection('content')