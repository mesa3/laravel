<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index');


Route::get('users/ajax', 'UserController@ajax');
Route::resource('users', 'UserController');


Route::get('courses/ajax', 'CourseController@ajax');
Route::resource('courses', 'CourseController');


Route::get('holidays/ajax', 'HolidayController@ajax');
Route::get('holidays/delete', 'HolidayController@destroy');
Route::resource('holidays', 'HolidayController');


Route::get('days/ajax', 'DayController@ajax');
Route::resource('days', 'DayController');

Route::get('classbooks/ajax', 'ClassbookController@ajax');
Route::resource('classbooks', 'ClassbookController');

Route::resource('classes', 'ClaseController');
Route::resource('clase', 'ClaseController');

Route::resource('units', 'UnitController');


Route::get('test/datepicker', function () {
    return view('datepicker/datepicker');
});


Route::get('/redirect/google', 'SocialAuthController@redirect');
Route::get('/callback/google', 'SocialAuthController@callback');


include "web/juan.php";
include "web/javier.php";
include "web/ernan.php";
