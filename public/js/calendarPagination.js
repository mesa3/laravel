/*
Estas funciones simplemente modifican las variables principales de calendarSetup para modificar donde
empieza y termina de mostrar los datos la función loadData

----------------------------------------------------------------------------------*/


/*Pagination*/
function next(){
    step = last;
    //console.log('FIRST', first, 'LAST', last, 'STEP', step);
    start = start + step;
    end = end + step;
    correct++;
    firstDay();
    //console.log('firstDay done');
    setTimeout(function(){
        lastDay();
        //console.log('lastDay done');
    }, 100);

    setTimeout(function(){
        loadData();
        //console.log('loadData done');
    }, 200); 
    
}
function prev(){
    step = last;
    //console.log('FIRST', first, 'LAST', last, 'STEP', step);
    start = start - step;
    end = end - step;
    correct--;
    firstDay();
    
    setTimeout(function(){
        lastDay();
        
    }, 100);

    setTimeout(function(){
        loadData();
        
    }, 200); 
}