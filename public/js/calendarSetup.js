/*
Del JSON recibo un String con este formato:
    
    ddmmyyyyDAY ddth of MONTH yyyy

De esta cadena se extraen todos los datos necesarios para rellenar la tabla del calendario en la que 
cada celda está identificada con:

    ROW+DAY(tres primeras letras)

El número ROW lo obtengo con la función weeek(i) para cada día de manera que concatenándolo con las
tres primeras letras del día optengo la posición de ese día para colocarlo en la tabla
*/


//-------------------------------------------------------------------------------------------------------------------------

/*Variables */
var start = 0;      //Se refiere al id del objeto en el JSON en el que empieza el mes
var end = 35;       //Se refiere al id del objeto en el JSON en el que acaba el mes
var correct = 0;    
var first = 0;      //Primer día del mes
var last = 35;      //Último día del mes
var step = 0;       //El número de días del mes para hacer la paginación y saber cuantos días hay que avanzar el JSON



/*Week - Calcula la semana en la que se coloca cada día, devolverá un valor entre 1 y 6*/
var week = function(i){
    var row = 0;
    i = i - step*correct + first;
    //console.log('first', first, 'i', i);

    for (var j = 1; j < 7; j++){
        if (((i /7) > (j-1)) && ((i / 7) <= j)){

            row = j;
            //console.log('row', row);
            return row;
        }
    }
}

/*Last day of the month - Calcula cual es el último día del mes para saber en que momento hay que dejar*/
var lastDay = function(){
    var cont = 0;
    url = '/days';
    $.get(url, function (data) {
        //first = 0;
        var data = data[0];
        for (var i = start; i < end; i++) {
            var dd = data[i].date.substr(0,2);
            var day = data[i].date.substr(8,3);
            //console.log('start', start, 'end', end);
            if(dd == '01'){
                cont ++;
                //console.log('CONTADOR', cont);
            }
            if (cont == 2){
                cont = 0;
                last = parseInt(data[i-1].date.substr(0,2));
                //console.log('LAST', last);
                //console.log('STEP', step);
            }
                     
        }
    }, 'json');
    console.log('lastDay done');
}

/*First day of the month - Obtiene el primer día del mes para saber que día de la semana es, devolverá un valor entre 1 y 7*/
var firstDay = function(){
    url = '/days';
    $.get(url, function (data) {
        //first = 0;
        var data = data[0];
        for (var i = start; i < end; i++) {
            var dd = data[i].date.substr(0,2);
            var day = data[i].date.substr(8,3);
            //console.log('start', start, 'end', end);
            if(dd == '01'){
                switch(day){
                    case "Mon": first = 1; break;
                    case "Tue": first = 2; break;
                    case "Wed": first = 3; break;
                    case "Thu": first = 4; break;
                    case "Fri": first = 5; break;
                    case "Sat": first = 6; break;
                    case "Sun": first = 7; break;
                }
                break;
            }
                     
        }
    }, 'json');
    console.log('firstDay done'); // Funciona como debería
}

/*Load Data List*/
var loadData = function () {
    console.log('FIRST', first, 'LAST', last, 'STEP', step);
    console.log('START', start, 'END', end);
    $("#tbodyMain").empty();
    url = '/days';
    $.get(url, function (data) {
        var data = data[0];
        //console.log(data);
        console.log(data[7]);

        var mm = data[start+7].date.split(" ");
        var year = data[start+7].date.substr(4,4);
        document.getElementById('month').innerHTML = mm[3];
        document.getElementById('year').innerHTML = year;

        for (var i = start; i < end; i++) {
            var dd = data[i].date.substr(0,2);
            var day = data[i].date.substr(8,3);
            
            var row = week(i);
            //console.log(row);
            var position = row + day;
            //console.log(week(i));
            //console.log('WEEK', week(i), 'POSITION', position);
            //console.log(data[i].id)
            
            $("#"+position).removeAttr('day_id');
            $('#'+position).attr('day_id', data[i].id.toString());
            
            if(data[i].holiday){
                $('#'+position).addClass('holiday');
            } else {
                $('#'+position).removeClass('holiday');
            }
            $('#'+position).text(dd);
        }


    }, 'json');
    console.log('loadData done');
};

/*Prepara cabeceras para envio del token csrf*/
$.ajaxSetup({
    headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});


firstDay();
//console.log('firstDay done');
setTimeout(function(){
    lastDay();
    //console.log('lastDay done');
    //console.log(step);
}, 100);

setTimeout(function(){
    loadData();
    //console.log('loadData done');
}, 200); 
