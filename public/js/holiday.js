/*
Cada vez que el usuario haga click en un día del calendario se mandará una petición a través de AJAX
para crear o borrar ese día como festivo de la base de datos.



--------------------------------------------------*/

/*ToggleHoliday - Da de alta/baja un día festivo*/
function toggleHoliday(element){
    $(element).toggleClass('holiday');
    if ($(element).hasClass('holiday')) {
        createHoliday(element);
    } else {
        removeHoliday(element);
    }
}


function createHoliday(element){
	console.log('create holiday');

	date = $(element).attr('day_id');

	$.post({
        url:   '../holidays',
        data:  { id: date },
	});
}

function removeHoliday(element){
	console.log('remove holiday');

	date = $(element).attr('day_id');

	$.get({
        url:   '../holidays/delete',
        data:  { id: date },
	});
}