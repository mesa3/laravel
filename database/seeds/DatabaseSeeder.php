<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(CoursesSeeder::class);
        $this->call(DaysSeeder::class);
        $this->call(HolidaysSeeder::class);
        $this->call(ClassesSeeder::class);
        $this->call(UnitsSeeder::class);
        $this->call(ClassbooksSeeder::class);
        
    }
}
