<?php

use Illuminate\Database\Seeder;

class HolidaysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1,10) as $index) {
            DB::table('holidays')->insert([
                'holiday' => true,
                'day_id' => mt_rand(1, 70)
            ]);
        }
    }
}
