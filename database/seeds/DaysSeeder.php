<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DaysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = Carbon::createFromDate(2016,8,31);
        foreach (range(1,302) as $index) {
            DB::table('days')->insert([
                'date' => $date->addDays(1),
                'day' => 1,
                'evening' => 1,
                'courses_id' => 1
            ]);
        }
    }
}