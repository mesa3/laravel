<?php

use Illuminate\Database\Seeder;

class UnitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('units')->insert([
	        'hours' => 10,        
	        'title' => 'Introducción PHP',
	        'user_id' => 2
	    ]);
	    DB::table('units')->insert([
	        'hours' => 10,
	        'title' => 'CRUD y tratamiento de datos',
	        'user_id' => 3
	    ]);
	    DB::table('units')->insert([
	        'hours' => 10,
	        'title' => 'Uso de Frameworks',
	        'user_id' => 3
	    ]);
	    DB::table('units')->insert([
	        'hours' => 10,
	        'title' => 'Laravel',
	        'user_id' => 3
	    ]);

	    //No puede haber una FoerignKey aquí, ya aparece en la tabla Classbooks. Si la ponemos aquí da conflicto en la creación. 
    }
}