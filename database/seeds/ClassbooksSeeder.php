<?php

use Illuminate\Database\Seeder;

class ClassbooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('classbooks')->insert([
	        'module' => 'Desarrollo de Aplicaciones en Entorno Servidor',
	        'group' => 'DAW2',
	        'comments' => 'Thats all folks!',
	        'day1' => 1,
	        'day2' => 1,
	        'day3' => 1,
	        'day4' => 1,
	        'day5' => 1,
	        'user_id' => 1,
			'class_id' => 1,
			'course_id' => 1,
			'unit_id' => 1
	    ]);
        DB::table('classbooks')->insert([
            'module' => 'Lenguaje de marcas',
            'group' => 'DAW1',
            'comments' => 'Thats all folks!',
            'day1' => 1,
            'day2' => 0,
            'day3' => 0,
            'day4' => 0,
            'day5' => 1,
            'user_id' => 2,
            'class_id' => 1,
            'course_id' => 1,
            'unit_id' => 1
        ]);

    }
}
