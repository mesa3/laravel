<?php

use Illuminate\Database\Seeder;

class ClassesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('classes')->insert([
	        'hours' => 10,
	        'plan' => 'LOE',
	        'done' => 'yes',
	        'date' => '2017-02-09',
            'user_id' => 1
	    ]);
	    DB::table('classes')->insert([
	        'hours' => 10,
	        'plan' => 'LOE',
	        'done' => 'yes',
	        'date' => '2017-02-10',
            'user_id' => 2
	    ]);
	    DB::table('classes')->insert([
	        'hours' => 10,
	        'plan' => 'LOE',
	        'done' => 'yes',
	        'date' => '2017-02-11',
            'user_id' => 2
	    ]);
	    DB::table('classes')->insert([
	        'hours' => 10,
	        'plan' => 'LOE',
	        'done' => 'yes',
	        'date' => '2017-02-12',
            'user_id' => 1
	    ]);
	    DB::table('classes')->insert([
	        'hours' => 10,
	        'plan' => 'LOE',
	        'done' => 'yes',
	        'date' => '2017-02-13',
            'user_id' => 3
	    ]);

	    //Creo que necesitamos tener aquí la clave ajena de unidad, si no es imposible saber a que materia pertenece

       
    }
}
