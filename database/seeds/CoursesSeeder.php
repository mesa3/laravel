<?php

use Illuminate\Database\Seeder;

class CoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert([
            'year'=>'2017',
            'date_ini'=>'2017-03-15',
            'date_end'=>'2017-06-15',

            ]);       
    }
}
