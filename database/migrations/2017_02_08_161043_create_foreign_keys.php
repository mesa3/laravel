<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('role_id')->default(2);
            $table->foreign('role_id')->references('id')->on('roles');
        });
        Schema::table('days', function (Blueprint $table) {
            /*$table->unsignedInteger('holiday_id');
            $table->foreign('holidays_id')->references('id')->on('holidays');*/
            $table->unsignedInteger('courses_id');
            $table->foreign('courses_id')->references('id')->on('courses');
        });
        Schema::table('holidays', function (Blueprint $table) {
            $table->unsignedInteger('day_id');
            $table->foreign('day_id')->references('id')->on('days');
        });
        /*Schema::table('units', function (Blueprint $table) {
            $table->unsignedInteger('classbook_id');
            $table->foreign('classbook_id')->references('id')->on('classbooks');
        });*/
        Schema::table('classbooks', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('class_id');
            $table->unsignedInteger('course_id');
            $table->unsignedInteger('unit_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('class_id')->references('id')->on('classes')->onDelete('cascade');
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
            $table->foreign('unit_id')->references('id')->on('units')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_role_id_foreign');
        });
        Schema::table('days', function (Blueprint $table) {
            //$table->dropForeign('days_holiday_id_foreign');
            $table->dropForeign('days_courses_id_foreign');
        });
        //return;
        Schema::table('holidays', function (Blueprint $table) {
            $table->dropForeign('holidays_day_id_foreign');
        });
        /*Schema::table('units', function (Blueprint $table) {
            $table->dropForeign('units_classbook_id_foreign');
        });*/
        Schema::table('classbooks', function (Blueprint $table) {
            $table->dropForeign('classbooks_user_id_foreign');
            $table->dropForeign('classbooks_class_id_foreign');
            $table->dropForeign('classbooks_course_id_foreign');
            $table->dropForeign('classbooks_unit_id_foreign');
        });
    }
}
