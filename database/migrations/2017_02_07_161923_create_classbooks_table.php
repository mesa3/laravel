<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassbooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classbooks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('module');
            $table->string('group');
            $table->string('comments');
            $table->binary('day1');
            $table->binary('day2');
            $table->binary('day3');
            $table->binary('day4');
            $table->binary('day5');
            $table->timestamps();
        });

        //Mejor las columnas dayX como binary para marcar si ese día a habido actividad o no. Como string puede ser confuso
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classbooks');
    }

}
