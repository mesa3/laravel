<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clase extends Model
{
    protected $fillable=['id', 'hours', 'plan', 'done', 'date', 'user_id'];
    protected $table = 'classes';
    
    public function classbooks()
    {
        return $this->hasMany('App\Classbook');
    }
}
