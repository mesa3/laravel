<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Course' => 'App\Policies\CoursePolicy',
        'App\Holiday' => 'App\Policies\HolidayPolicy',
        'App\User' => 'App\Policies\UserPolicy',
        'App\Clase' => 'App\Policies\ClasesPolicy',
        'App\Classbook' => 'App\Policies\ClassbookPolicy',
        'App\Unit' => 'App\Policies\UnitPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
