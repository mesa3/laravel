<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable = ['hours', 'title', 'user_id'];

    public function classbooks()
    {
        return $this->hasMany('App\Classbook');
    }
}
