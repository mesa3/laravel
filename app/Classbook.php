<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classbook extends Model
{
    protected $fillable = ['module', 'group', 'comments', 'day1', 'day2', 'day3', 'day4', 'day5', 'user_id', 'course_id', 'class_id', 'unit_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function course()
    {
        return $this->belongsTo('App\Course');
    }

     public function class()
     {
        return $this->belongsTo('App\Clase');

     }
    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }
}
