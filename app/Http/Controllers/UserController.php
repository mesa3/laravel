<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;


class UserController extends Controller
{
	
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        
        $userlist = new User();
        $user = $request->user();
        if (! $user->can('view', $userlist)) {
            return redirect('/home');
        }
        $users = User::paginate(5);
        if ($request->ajax()) {
            return $users;
        } else {
            return view('user.index', ['users' => $users]);
        }
    }


    public function ajax()
    {
        return view('user.ajax');
    }

    
    public function create()
    {
        $this->authorize('create', User::class);
        return view('auth.register');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
        'name' => 'required|max:10',
        'surname' => 'max:20',
        'email' => 'required|email',
        'password' => 'required|max:20'
        ]);

        $user = new User($request->all());
        $user->save();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/users');
        }
    }

    public function show($id, Request $request)
    {
        $userlist = new User();
        $user = User::findOrFail($id);
        $this->authorize('view', $userlist);
        if ($request->ajax()) {
            return $user;
        } else {
            return view('user.show', ['user' => $user]);
        }
    }

    public function edit($id)
    {
        $userlist = new User();
        $user = User::findOrFail($id);
        $this->authorize('update', $userlist);
        return view('user.edit', ['user' => $user]);
    }

    public function update(Request $request, $id)
    {
       
        $this->validate($request, [
        'name' => 'required|max:10',
        'surname' => 'max:20',
        'email' => 'required|email',
        'password' => 'required|max:20'
        ]);

        $userlist = new User();
        $user = User::findOrFail($id);
        $this->authorize('update', $userlist);
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->email = $request->email;
        $user->save();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/users');
        }
    }

    public function destroy($id, Request $request)
    {
        $userlist = new User();
        User::find($id)->delete();
        $this->authorize('delete', $userlist);
        
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/users');
        }
    }
}