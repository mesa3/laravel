<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clase;
use App\User;
use Auth;

class ClaseController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $user = $request->user();
        if (is_null($user) || $user->isUser()) {
            $classes = Clase::where('user_id', $user->id)->paginate(5);
        } else {
            $classes = Clase::paginate(5);

        }
        return view('clase.index', ['clases' => $classes]);
    }

    public function ajax()
    {
        return view('clase.ajax');
    }

    public function create()
    {
        
        $user_id = Auth::user()->id;

        $this->authorize('create', Clase::class);
        return view('clase.create', ['user_id' => $user_id]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
        'hours' => 'numeric',
        ]);
        
        $clase = new Clase(array_merge(['done'=>'no', ], $request->all()));

        $clase->save();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/classes');
        }
    }

    public function show($id, Request $request)
    {
        $clase = Clase::findOrFail($id);
        $this->authorize('view', $clase);
        if ($request->ajax()) {
            return $clase;
        } else {
            return view('clase.show', ['clase' => $clase]);
        }
    }

    public function edit($id)
    {
        $clase = Clase::findOrFail($id);
        $this->authorize('update', $clase);
        return view('clase.edit', ['clase' => $clase]);
    }

    public function update(Request $request, $id)
    {      

        $this->validate($request, [
        'hours' => 'numeric',
        ]);

        $clase = Clase::findOrFail($id);
        $this->authorize('update', $clase);
        $clase->done = $request->done;
        $clase->hours = $request->hours;
        $clase->plan = $request->plan;
        $clase->fill(array_merge(['done'=>'no',], $request->all()));
        $clase->save();

        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/classes');
        }
    }

    public function destroy($id, Request $request)
    {
        $clase = Clase::findOrFail($id);
        $this->authorize('delete', $clase);
        Clase::destroy($id);
        return redirect('/classes');
    }
}
