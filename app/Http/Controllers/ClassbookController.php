<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classbook;
use App\User;
use App\Course;
use App\Clase;
use App\Unit;
use Auth;

class ClassbookController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        $user = $request->user();
        if (is_null($user) || $user->isUser()) {
            $classbooks = Classbook::where('user_id', $user->id)->paginate();
        } else {
            $classbooks = Classbook::paginate(5);

        }
        return view('classbook.index', ['classbooks' => $classbooks]);
        
    }

    public function create()
    {
        $this->authorize('create', Classbook::class);
        $user_id = Auth::user()->id;
        $courses = Course::all();
        $classes = Clase::where('user_id', '=', $user_id)->get();
        $units = Unit::where('user_id', '=', $user_id)->get();

        return view('classbook.create', ['courses' => $courses, 'user_id' => $user_id, 'classes' => $classes, 'units' => $units]);
    }
 
    public function store(Request $request)
    {

        $classbook = new Classbook(array_merge(['day1'=>'0', 'day2'=>'0',  'day3'=>'0', 'day4'=>'0', 'day5'=>'0', ], $request->all()));

        $classbook->save();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/classbooks');
        }
    }

    public function edit($id)
    {
        $classbook = Classbook::findOrFail($id);
        $courses = Course::all();
        $classes = Clase::all();
        $units = Unit::all();
        $this->authorize('update', $classbook);
        return view('classbook.edit', ['classbook' => $classbook, 'courses' => $courses, 'classes' => $classes, 'units' => $units]);
    }

    
    public function update(Request $request, $id)
    {

        $classbook = Classbook::findOrFail($id);
        $this->authorize('update', $classbook);
        //$classbook->user_id = $request->id;
        $classbook->module = $request->module;
        $classbook->group = $request->group;
        $classbook->comments = $request->comments;
        $classbook->day1 = $request->day1;
        $classbook->day2 = $request->day2;
        $classbook->day3 = $request->day3;
        $classbook->day4 = $request->day4;
        $classbook->day5 = $request->day5;
        
        $classbook->fill(array_merge(['day1'=>'0', 'day2'=>'0',  'day3'=>'0', 'day4'=>'0', 'day5'=>'0', ], $request->all()));
        
        $classbook->user_id = $request->user()->id;
        
        $classbook->save();
        
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/classbooks');
        }
    }
    public function show($id, Request $request)
    {
        $classbook = Classbook::findOrFail($id);
        $this->authorize('view', $classbook);
        if ($request->ajax()) {
            return $classbook;
        } else {
            return view('classbook.show', ['classbook' => $classbook]);
        }
    }

    public function destroy($id)
    {
        $classbook = Classbook::findOrFail($id);
        $this->authorize('delete', $classbook);
        Classbook::destroy($id);
        return redirect('/classbooks');
    }
}
