<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unit;
use App\Classbook;
use App\User;
use Auth;

class UnitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        $user = $request->user();
        if (is_null($user) || $user->isUser()) {
            $units = Unit::where('user_id', $user->id)->paginate();
        } else {
            $units = Unit::paginate(5);

        }
        return view('unit.index', ['units' => $units]);


    }

    public function create()
    {


        $this->authorize('create', Unit::class);
        $user_id = Auth::user()->id;
       
        return view('unit.create', ['user_id' => $user_id]);
    }


    public function store(Request $request)
    {

        $this->validate($request, [
        'hours' => 'numeric',
        ]);

        $unit = new Unit($request->all());
        $unit->save();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/units');
        }
    }


    public function show($id, Request $request)
    {
        $unit = Unit::findOrFail($id);
        $this->authorize('view', $unit);
        if ($request->ajax()) {
            return $unit;
        } else {
            return view('unit.show', ['unit' => $unit]);
        }
    }


    public function edit($id)
    {
        $unit = Unit::findOrFail($id);
        $this->authorize('update', $unit);
        return view('unit.edit', ['unit' => $unit]);
    }


    public function update(Request $request, $id)
    {

        $this->validate($request, [
        'hours' => 'numeric',
        ]);
        
        $unit = Unit::findOrFail($id);
        $this->authorize('update', $unit);
        $unit->hours = $request->hours;
        $unit->number = $request->number;
        $unit->title = $request->title;
        $unit->save();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/units');
        }
    }


    public function destroy($id, Request $request)
    {
        $unit = Unit::findOrFail($id);
        $this->authorize('delete', $unit);
        Unit::destroy($id);
        return redirect('/units');
    }
}
