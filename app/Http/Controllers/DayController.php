<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Day;
use Carbon\Carbon;

class DayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $day = new Day();
        $user = $request->user();
        /*if (! $user->can('view', $day)) {
            return redirect('/manage');
        }*/

        $days = Day::with('holiday')->get();

        foreach ($days as $day) {
	        $yyyy = substr($day->date, 0, 4);
	        $mm = substr($day->date, 5, 2);
	        $dd = substr($day->date, 8, 2);
	        $day->date = Carbon::createFromDate($yyyy, $mm, $dd, 'Europe/London');
	        $day->date = $dd . $mm . $yyyy . $day->date->format('l jS \\of F Y');
	    }
	    //return $days;
        if ($request->ajax()) {
            return response()->json([$days]);
        } else {
            return view('day.index', ['days' => $days]);
        }
    }

    public function ajax()
    {
        return view('day.ajax');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Para poder escoger el día desde un calendario en el formulario
        //https://styde.net/formulario-con-datepicker-en-laravel/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'holiday' => 'required|max:1',
        ]);

        $holiday = new Holiday($request->all());
        $holiday->save();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/holidays');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        Holiday::find($id)->delete();
        $this->authorize('delete', $holiday);
        //Family::destroy($id);
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/holidays');
        }
    }
}
