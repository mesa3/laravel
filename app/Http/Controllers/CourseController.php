<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;

class CourseController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $course = new Course();
        $user = $request->user();
        if (! $user->can('view', $course)) {
            return redirect('/home');
        }
      
        $courses = Course::paginate(5);
        if ($request->ajax()) {
            return $courses;
        } else {
            return view('course.index', ['courses' => $courses]);
        }
    }


    public function ajax()
    {
        return view('course.ajax');
    }

    public function create()
    {
        $this->authorize('create', Course::class);
        return view('course.create');
    }

   
    public function store(Request $request)
    {
        $this->validate($request, [
        'year' => 'required|max:4',
        'date_ini' => 'required|max:10',
        'date_end' => 'required|max:10',
        ]);

        $course = new Course($request->all());
        $course->save();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/courses');
        }
    }

   
    public function show($id, Request $request)
    {
        $course = Course::findOrFail($id);
        $this->authorize('view', $course);
        if ($request->ajax()) {
            return $course;
        } else {
            return view('course.show', ['course' => $course]);
        }
    }

   
    public function edit($id)
    {
        $course = Course::findOrFail($id);
        $this->authorize('update', $course);
        return view('course.edit', ['course' => $course]);
    }

   
    public function update(Request $request, $id)
    {
       
        $this->validate($request, [
        'year' => 'required|max:4',
        'date_ini' => 'required|max:10',
        'date_end' => 'required|max:10',
        ]);

        $course = Course::findOrFail($id);
        $this->authorize('update', $course);
        $course->year = $request->year;
        $course->date_ini = $request->date_ini;
        $course->date_end = $request->date_end;
        $course->save();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/courses');
        }
    }

    
    public function destroy($id, Request $request)
    {
        $course = Course::findOrFail($id);
        $this->authorize('delete', $course);
        Course::destroy($id);
        return redirect('/courses');
    }
}
