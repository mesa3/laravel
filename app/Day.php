<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    public function holiday()
    {
    	return $this->hasOne('App\Holiday');
    }
}
