<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['year', 'date_ini', 'date_end'];

    public function days()
    {
        return $this->hasMany('App\Day');
    }

    public function classbooks()
    {
        return $this->hasMany('App\Classbook');
    }
}
