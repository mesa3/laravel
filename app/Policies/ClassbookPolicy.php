<?php

namespace App\Policies;

use App\User;
use App\Classbook;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClassbookPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return $user->isUser();
    }

    public function update(User $user, Classbook $classbook)
    {
        return $user->isUser();
    }
    public function view(User $user, Classbook $classbook)
    {
        return true;
    }
    public function delete(User $user, Classbook $classbook)
    {
        return $user->isUser();
        
    }

}
