<?php

namespace App\Policies;

use App\User;
use App\Unit;
use Illuminate\Auth\Access\HandlesAuthorization;

class UnitPolicy
{
    use HandlesAuthorization;
    
    public function view(User $user, Unit $unit)
    {
        return true;
    }

    public function create(User $user)
    {
        return $user->isUser();
    }

    public function update(User $user, Unit $unit)
    {
         return $user->isUser();
    }


    public function delete(User $user, Unit $unit)
    {
        return $user->isUser();   
    }
}
