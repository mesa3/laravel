<?php

namespace App\Policies;

use App\User;
use App\Clase;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClasesPolicy
{
    use HandlesAuthorization;
    
    public function view(User $user, Clase $clase)
    {
        return true;
    }

    public function create(User $user)
    {
        return $user->isUser();
    }

    public function update(User $user, Clase $clase)
    {
        return $user->isUser();
    }

    public function delete(User $user, Clase $clase)
    {
        return $user->isUser();
    }
}
