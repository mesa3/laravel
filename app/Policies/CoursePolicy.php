<?php

namespace App\Policies;

use App\User;
use App\Course;
use Illuminate\Auth\Access\HandlesAuthorization;

class CoursePolicy
{
    use HandlesAuthorization;
    
    public function view(User $user, Course $course)
    {
        return true;
        // return $user->isAdmin();
    }

    public function create(User $user)
    {
        //return true;
        return $user->isAdmin();
    }

    public function update(User $user, Course $course)
    {
        //return true;
        return $user->isAdmin();
    }

    public function delete(User $user, Course $course)
    {
        //return true;
        return $user->isAdmin();
    }
}
