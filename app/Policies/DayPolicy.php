<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DayPolicy
{
    use HandlesAuthorization;
    
    public function view(User $user, Day $day)
    {
        return true;
        // return $user->isAdmin();
    }

    public function create(User $user)
    {
        //return true;
        return $user->isAdmin();
    }

    public function delete(User $user, Day $day)
    {
        //return true;
        return $user->isAdmin();
    }
}

