<?php

namespace App\Policies;

use App\User;
use App\Holiday;
use Illuminate\Auth\Access\HandlesAuthorization;

class HolidayPolicy
{
    use HandlesAuthorization;
    
    public function view(User $user, Holiday $holiday)
    {
        return true;
        // return $user->isAdmin();
    }

    public function create(User $user)
    {
        //return true;
        return $user->isAdmin();
    }

    public function delete(User $user, Holiday $holiday)
    {
        //return true;
        return $user->isAdmin();
    }
}
