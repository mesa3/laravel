<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;
    
    public function view(User $user, User $userlist)
    {
        return $user->isAdmin();
        //return true;
    }

    public function create(User $user)
    {
        //return true;
        return $user->isAdmin();
    }

    public function update(User $user, User $userlist)
    {
        //return true;
        return $user->isAdmin();
    }

    public function delete(User $user, User $userlist)
    {
        //return true;
        return $user->isAdmin();
    }
}
